# Linux Anleitungen

## Aktive Linux Distribution ermitteln
Wenn Sie nicht wissen, welche Linux-Distribution Sie verwenden, gibt es einen einfachen Befehl, den Sie in Ihrem Terminal eingeben können.

Öffnen Sie dazu ein Terminal und geben Sie ein: `sudo cat /etc/os-release`.
Mit diesem Befehl erhalten Sie eine Ausgabe des Namens und der `ID_LIKE` Ihrer Distribution.

Meine sieht so aus:
```
NAME="Manjaro Linux"
ID=manjaro
ID_LIKE=arch
BUILD_ID=rolling
PRETTY_NAME="Manjaro Linux"
ANSI_COLOR="32;1;24;144;200"
HOME_URL="https://manjaro.org/"
DOCUMENTATION_URL="https://wiki.manjaro.org/"
SUPPORT_URL="https://manjaro.org/"
BUG_REPORT_URL="https://bugs.manjaro.org/"
LOGO=manjarolinux
```

Was sich aus diesen Informationen klar herauslesen lässt, ist, dass ich `Manjaro Linux` als Hauptdistribution verwende.

Wenn Sie Ubuntu verwenden, würde es ähnlich wie hier aussehen:

```
NAME="Ubuntu" PRETTY_NAME="Ubuntu Bionic" ID=ubuntu ID_LIKE=debian
```

## Windows Product-Key mit chntpw auslesen
Zuerst müssen Sie die Registrierung Ihrer Windows-Installation ermitteln. Normalerweise befindet sie sich unter `C:\Windows\System32\config`.

Um den Produkt-Schlüssel zu erhalten, müssen Sie die `SOFTWARE`-Datei lesen.

Installieren Sie dazu `chntpw` mit dem entsprechenden Paketmanager Ihrer aktuellen Linux-Distribution, in meinem Fall gebe ich `sudo pacman -S chntpw` in meinem Terminal ein.
![](/img/chntpw_terminal.png)

Wenn Sie nicht wissen, welche Linux-Distro Sie verwenden, werfen Sie einen Blick auf den Teil [**Aktive Linux Distribution**]() ermitteln.

Öffnen Sie nun ein Terminal im config-Ordner der externen Festplatte und geben Sie

```
sudo chntpw -e SOFTWARE
```

in Ihrem Terminal ein.

Daraufhin öffnet sich ein minimaler Registrierungseditor.

Geben Sie nun `dpi \Microsoft\Windows NT\CurrentVersion\DigitalProductId` in diesem Registrierungseditor ein.

Dies wird nun die Produkt-ID im folgenden Format ausgeben:
```
Value <\Microsoft\Windows NT\CurrentVersion\DigitalProductId> of
type REG_BINARY, data length 164 [0xa4] Decoded product ID:
[XXXXXXXXXXXXXXXXXXXXXXXXX]
```
Die X's stehen für den dekodierten Produktschlüssel.