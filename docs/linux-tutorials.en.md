# Linux Guides

## Get active Linux Distro
If you don’t know which Linux-Distribution you are using, there’s a simple command, which you can type inside your terminal.

To do so, open up a terminal and type: `sudo cat /etc/os-release` this will give you an output of the name and the `ID_LIKE` of your Distribution.

Mine looks like this:
```
NAME="Manjaro Linux"
ID=manjaro
ID_LIKE=arch
BUILD_ID=rolling
PRETTY_NAME="Manjaro Linux"
ANSI_COLOR="32;1;24;144;200"
HOME_URL="https://manjaro.org/"
DOCUMENTATION_URL="https://wiki.manjaro.org/"
SUPPORT_URL="https://manjaro.org/"
BUG_REPORT_URL="https://bugs.manjaro.org/"
LOGO=manjarolinux
```

What can clearly read out of this information is, that I’m using `Manjaro Linux` as my main distribution.

If you’re using Ubuntu, it would look similar to this:

```
NAME="Ubuntu" PRETTY_NAME="Ubuntu Bionic" ID=ubuntu ID_LIKE=debian
```

## Get Windows Product-Key with chntpw
First you have to locate the registry of your windows installation. Normally it’s located at `C:\Windows\System32\config`.

To get the product-key, you have to read the `SOFTWARE` file.

To do this, install `chntpw` with the appropriate package manager of you current linux distribution, In my case I’m typing `sudo pacman -S chntpw` inside my terminal.

![](/img/chntpw_terminal.png)

If you don’t know which linux-distro you’re using, take a look at the part [**Get active Linux Distro**]().

Now, open up a terminal inside the config folder of the external harddrive, and type

```
sudo chntpw -e SOFTWARE
```

This will open up a minimal registry editor.

Now type: `dpi \Microsoft\Windows NT\CurrentVersion\DigitalProductId`

This will now output the Product ID in the following format:
```
Value <\Microsoft\Windows NT\CurrentVersion\DigitalProductId> of
type REG_BINARY, data length 164 [0xa4] Decoded product ID:
[XXXXXXXXXXXXXXXXXXXXXXXXX]
```
The X’s represent the decoded product key.